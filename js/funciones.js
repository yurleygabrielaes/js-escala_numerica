function crearEscala()
{
  
  var numero=document.getElementById("entrada").value; // pide el número al usuario
  var rta=crearOperacion(0,numero); // empieza multiplicando el primer número que digita el usuario
  var rta2=crearOperacion(1,rta[0]); // aquí empieza a dividir con el último resultado de la multiplicación
  
  var tabla="<table class='center'>"; // crea la tabla
  tabla+=
  "<tr>"+
    "<th></th>" +
    "<th>ESCALA DEL NÚMERO: " + numero + "</th>" +
    "<th></th>"+
  "</tr>" +
  "<tr>" +
    "<th></th>"+
    "<th>Operación multiplicar </th>"+
    "<th></th>"+
  "</tr>"+
  "<tr>"+ // crea una fila
    "<th> Dato 1 </th>"+ // crea columna operación
    "<th> Dato 2 </th>"+
    "<th> Resultado</th>"+ // crea columna resultado
  "</tr>"; // cierra fila


// primero pone la rta 
  tabla+=rta[1];
  tabla+="<tr>" +
    "<th></th>"+
    "<th>Operación dividir </th>"+
    "<th></th>"+
  "</tr>";

  tabla+=rta2[1];

  tabla+="</table>"; // cierra la tabla

  
  document.getElementById("rta3").innerHTML=tabla; // pone la tabla donde esta el id = "rta3"
}


function crearOperacion(operacion,numero)
{

  let m=["*","/"]; // para mostrar en pantalla el signo de la operación
  let vectorRta = new Array(2);
  var rta="";

  for(var i=1;i<=10;i++)
  {
   
   var operacionResultado=0;
   if(operacion==0) // si la operación es una multiplicación
    operacionResultado=numero*i;
  else // si la operación es una división
    operacionResultado=numero/i;

  var mensaje=numero; // mensaje que vemos en pantalla

  numero=operacionResultado; //numero --> guarda el resultado

  rta+="\n<tr>"; // abre una fila
  rta+="\n<td style='text-align:center;'>"+mensaje+"</td>"; // en esta columna muestra el mensaje
  rta+="\n<td style='text-align:center;'>"+i+"</td>";
  rta+="\n<td style='text-align:center;'>"+operacionResultado+"</td>"; // en esta columna muestra el resultado
  rta+="\n</tr>"; // cierra una fila
}

vectorRta[0]=numero; //aqúí llega hasta el último resultado de la multiplicación 
vectorRta[1]=rta; 
return vectorRta;
}

